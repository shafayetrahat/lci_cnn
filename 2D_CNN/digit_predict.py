# Importing the Keras libraries and packages
from keras.models import load_model
model = load_model('models/mnistCNN.h5')

import numpy as np
import cv2

# for index in range(13):


def predict(img):
    print("Predicting......")
    # print(index)
    image_width = 28
    image_height = 28
    dim = (image_width, image_height)
    # img = cv2.imread('data/' + "12" + '.png', cv2.IMREAD_UNCHANGED)
    img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    #

    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # (thresh, img) = cv2.threshold(img, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    img = cv2.bitwise_not(img)
    # test_gray = test_gray/255.0
    # cv2.imshow("input",img)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    img = img/255.0
    # img = Image.open('data/' + str(index) + '.png').convert("L")
    # img = img.resize((28,28))
    # print(img)
    im2arr = np.array(img)
    im2arr = im2arr.reshape(1,28,28,1)

    # print(im2arr)
    # Predicting the Test set results
    y_pred = model.predict(im2arr)
    # print(y_pred)
    return np.argmax(y_pred)
