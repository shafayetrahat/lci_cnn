import numpy as np
import cv2
import math
import digit_predict as dp
import csv
# Load and then gray scale image


tlX = 0
tlY = 0
trX = 0
trY = 0
blX = 0
blY = 0
brX = 0
brY = 0

imgName = "scan0002.bmp"
print(imgName)
img = cv2.imread(imgName)
image = cv2.medianBlur(img,51)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
gHeight = gray.shape[0]
gWidth = gray.shape[1]

w = 700
h = 400

gHeightN = gHeight-h
gWidthN = gWidth-w

crop_img1 = gray[0:0+h, 0:0+w]
crop_img2 = gray[0:0+h, gWidthN: gWidth]
crop_img3 = gray[gHeightN:gHeight, 0: 0+w]
crop_img4 = gray[gHeightN:gHeight, gWidthN:gWidth]
img2 = cv2.imread('starb.jpg')
gray2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)


# ##################################################
thresh = cv2.adaptiveThreshold(crop_img1, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
ret, thresh2 = cv2.threshold(gray2, 0, 255, cv2.THRESH_BINARY)

contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

matchContour, hierarchy2 = cv2.findContours(thresh2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

cntM = matchContour[0]

for cnt in contours:
    ret = cv2.matchShapes(cnt, cntM, 1, 0.0)
    M = cv2.moments(cnt)
    if cv2.arcLength(cnt, True) != 0.0:
        SM = math.sqrt(M['m00'] / cv2.arcLength(cnt, True))
    else:
        continue
    approx = cv2.approxPolyDP(cnt, 0.1 * cv2.arcLength(cnt, True), True)
    # print(SM)
    if ret <= 0.07 and len(approx) == 4 and 2.5<= SM <= 3.3:
        CX = M['m10'] / M['m00']
        CY = M['m01'] / M['m00']
        tlX = int(CX)
        tlY = int(CY)

        cv2.putText(image, "star", (tlX - 50, tlY), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 190, 0), 3)
        # print(ret)
        # print(cx)
        # print(cy)
#####################################################################################
ret, thresh = cv2.threshold(gray, 0,255,cv2.THRESH_BINARY_INV)
thresh = cv2.adaptiveThreshold(crop_img2, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
ret, thresh2 = cv2.threshold(gray2, 0, 255, cv2.THRESH_BINARY)

contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

matchContour, hierarchy2 = cv2.findContours(thresh2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

cntM = matchContour[0]

for cnt in contours:
    ret = cv2.matchShapes(cnt, cntM, 1, 0.0)
    M = cv2.moments(cnt)
    if cv2.arcLength(cnt, True) != 0.0:
        SM = math.sqrt(M['m00'] / cv2.arcLength(cnt, True))
    else:
        continue
    approx = cv2.approxPolyDP(cnt, 0.1 * cv2.arcLength(cnt, True), True)
    # print(SM)
    if ret <= 0.05 and len(approx) == 4 and 2.5<= SM <= 3.3:
        CX = M['m10'] / M['m00']
        CY = M['m01'] / M['m00']
        trX = int(gWidthN+CX)
        trY = int(CY)
        cv2.putText(image, "star", (trX - 50, trY), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 190, 0), 3)
        # print(ret)
        # print(cx)
        # print(cy)

#####################################################################################
ret, thresh = cv2.threshold(gray, 0,255,cv2.THRESH_BINARY_INV)
thresh = cv2.adaptiveThreshold(crop_img3, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
ret, thresh2 = cv2.threshold(gray2, 0, 255, cv2.THRESH_BINARY)

contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

matchContour, hierarchy2 = cv2.findContours(thresh2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

cntM = matchContour[0]

for cnt in contours:
    ret = cv2.matchShapes(cnt, cntM, 1, 0.0)
    M = cv2.moments(cnt)
    if cv2.arcLength(cnt, True) != 0.0:
        SM = math.sqrt(M['m00'] / cv2.arcLength(cnt, True))
    else:
        continue
    approx = cv2.approxPolyDP(cnt, 0.1 * cv2.arcLength(cnt, True), True)
    # print(SM)
    if ret <= 0.05 and len(approx) == 4 and 2.5<= SM <= 3.3:
        CX = M['m10'] / M['m00']
        CY = M['m01'] / M['m00']
        blX = int(CX)
        blY = int(CY+gHeightN)
        cv2.putText(image, "star", (blX - 50, blY), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 190, 0), 3)
        # print(ret)
        # print(cx)
        # print(cy)
#####################################################################################

ret, thresh = cv2.threshold(gray, 0,255,cv2.THRESH_BINARY_INV)
thresh = cv2.adaptiveThreshold(crop_img4, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
ret, thresh2 = cv2.threshold(gray2, 0, 255, cv2.THRESH_BINARY)

contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

matchContour, hierarchy2 = cv2.findContours(thresh2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

cntM = matchContour[0]

for cnt in contours:
    ret = cv2.matchShapes(cnt, cntM, 1, 0.0)
    M = cv2.moments(cnt)
    if cv2.arcLength(cnt, True) != 0.0:
        SM = math.sqrt(M['m00'] / cv2.arcLength(cnt, True))
    else:
        continue
    approx = cv2.approxPolyDP(cnt, 0.1 * cv2.arcLength(cnt, True), True)
    # print(SM)
    if ret <= 0.05 and len(approx) == 4 and 2.5<= SM <= 3.3:
        CX = M['m10'] / M['m00']
        CY = M['m01'] / M['m00']
        brX = int(CX+gWidthN)
        brY = int(CY+gHeightN)
        cv2.putText(image, "star", (brX - 50, brY), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 190, 0), 3)
        # print(ret)
        # print(cx)
        # print(cy)


#####################################################################################
# x1 = min(tlX, trX, brX, blX) # top - left pt. is the leftmost of the 4 points
# x2 = max(tlX, trX, brX, blX) # bottom - right pt. is the rightmost of the 4 points
# y1 = min(tlY, trY, brY, blY) # top-left pt.is the uppermost of the 4 points
# y2 = max(tlY, trY, brY, blY) # bottom-right pt.is the lowermost of the 4 points
herbo = cv2.medianBlur(img,5)

cv2.rectangle(herbo,(tlX,tlY),(brX,brY),(0,255,0),3)
#####################################################################################
scale_percent = 25  # percent of original size
width = int(image.shape[1] * scale_percent / 100)
height = int(image.shape[0] * scale_percent / 100)


dim = (width, height)
pts1 = np.float32([[tlX, tlY], [trX, trY], [blX, blY]])
pts2 = np.float32([[0, 0], [img.shape[1], 0], [0, img.shape[0]]])

M = cv2.getAffineTransform(pts1, pts2)
# print(M)
dst = cv2.warpAffine(herbo,M,(img.shape[1],img.shape[0]))

cw = int(dst.shape[1])
ch = int(dst.shape[0]-dst.shape[0]*.0152)

grid_h = int(ch/22)
grid_w = int(cw/15)
# resize image
# resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
# cv2.imshow('Identifying Shapes',resized)

# tresized = cv2.resize(thresh, dim, interpolation=cv2.INTER_AREA)
# cv2.imshow('Shapes',tresized)
# print("width")
# print(cw)
# print("height")
# print(ch)

# cv2.imwrite("out/"+imgName, img)
crop_img = dst[ch+15-(grid_h*9):ch-(grid_h*9)+grid_h+15, 0+15:grid_h+15]

predictionD = dp.predict(crop_img)
print(predictionD)
cv2.imshow("cropped", crop_img)
# # cv2.imwrite("dst/"+imgName,dst)
cv2.waitKey(0)
cv2.destroyAllWindows()
