import tensorflow as tf
import numpy as np
import cv2
#
mnist = tf.keras.datasets.mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()

train_mask = np.isin(y_train, [0, 1, 2])
test_mask = np.isin(y_test, [0, 1, 2])

x_train, y_train = x_train[train_mask], y_train[train_mask]
x_test, y_test = x_test[test_mask], y_test[test_mask]

x_train, x_test = x_train /255, x_test / 255
# print(fx_train[1])
# print(x_train[0].shape)
model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(512, activation=tf.nn.relu),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(10, activation=tf.nn.softmax)
])
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(x_train, y_train, epochs=2)
model.evaluate(x_test, y_test)


image_width =28
image_height = 28
dim = (image_width, image_height)
test = cv2.imread('31_06540.png',cv2.IMREAD_UNCHANGED)
test_image = cv2.resize(test, dim, interpolation = cv2.INTER_AREA)
#

test_gray = cv2.cvtColor(test_image, cv2.COLOR_BGR2GRAY)
# (thresh, im_bw) = cv2.threshold(test_gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

test_gray = test_gray/(255.0*255.0)
# cv2.imshow("input",test_gray)
# test_gray = im_bw
# cv2.imshow("rr",test_gray)
# cv2.imshow("hh",fx_train[1])
# print("####################################################")
# print(test_gray)

# cv2.waitKey(0)
# cv2.destroyAllWindows()
# print(test_gray.shape)
result_image = test_gray.reshape(1,28,28)
# result = model.predict_classes(result_image)
result = model.predict(test_gray.reshape(1,28,28))
# # print(type(test_gray))

# print(np.argmax(result[0]))
print(result)